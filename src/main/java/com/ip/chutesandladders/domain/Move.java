package com.ip.chutesandladders.domain;

public class Move {
    private final Integer nextPosition;
    private final Integer finalPoition;

    public Move(Integer nextPosition, Integer finalPosition) {
        this.nextPosition = nextPosition;
        this.finalPoition = finalPosition;
    }

    public Integer getNextPosition() {
        return nextPosition;
    }

    public Integer getFinalPosition() {
        return finalPoition;
    }
}
