package com.ip.chutesandladders.domain;

import org.apache.commons.lang3.Validate;

public class Player {
    private final String name;
    private final Integer initialSpinnerValue;
    private Integer pawnPosition;

    public Player(String name, Integer spinnerValue) {
        Validate.notBlank(name, "Every human has name");
        Validate.isTrue(spinnerValue != null && spinnerValue.compareTo(0) != 0,
                "How would I know who should go first without spinning the spinner?");

        this.name = name;
        initialSpinnerValue = spinnerValue;
        pawnPosition = 0;
    }

    public String getName() {
        return name;
    }

    public Integer getInitialSpinnerValue() {
        return initialSpinnerValue;
    }

    public Integer getPawnPosition() {
        return pawnPosition;
    }

    public Move movePawnForTheFirstTime(Board board) {
        Validate.validState(pawnPosition == 0, "First time means first time");

        Move move = moveInternal(board, getInitialSpinnerValue());
        pawnPosition = move.getFinalPosition();

        return move;
    }

    public Move movePawn(Board board, Spinner spinner) {
        Move move = moveInternal(board, spinner.doTheMagic());
        pawnPosition = move.getFinalPosition();

        return move;
    }

    private Move moveInternal(Board board, Integer spinnerValue) {
        Integer nextPosition = board.nextImmediatePosition(pawnPosition, spinnerValue);
        Integer finalPosition = board.nextPositionIfChuteOrLadderUsed(pawnPosition, spinnerValue);

        return new Move(nextPosition, finalPosition);
    }
}