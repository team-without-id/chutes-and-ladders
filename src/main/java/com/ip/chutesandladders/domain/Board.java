package com.ip.chutesandladders.domain;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class Board {
    public static final String LADDERS_SEPARATOR = ",";
    public static final String ENTRANCE_EXIT_SEPARATOR = StringUtils.SPACE;

    public static final Integer START_SQUARE = 0;
    public static final Integer FINAL_SQUARE = 100;

    private static final String DEFAULT_LADDERS_CONFIGURATION =
            "1 38, 4 14, 9 31, 16 6, 28 84, 36 44, 47 26, 49 11, 51 67, 56 53, 62 19, 64 60, 71 91, 80 100, 87 24, 93 73, 95 75, 98 78";

    private final Map<Integer, Integer> tunnels;

    public Board() {
        tunnels = parseConfiguration(DEFAULT_LADDERS_CONFIGURATION);
    }

    public Board(String laddersConfiguration) {
        if (StringUtils.isBlank(laddersConfiguration))
            tunnels = new HashMap<>();
        else
            tunnels = parseConfiguration(laddersConfiguration);
    }

    private Map<Integer, Integer> parseConfiguration(String laddersConfiguration) {
        Map<Integer, Integer> tunnels = new HashMap<>();
        try {
            for (String ea : laddersConfiguration.split(LADDERS_SEPARATOR)) {
                String[] entranceExit = ea.trim().split(ENTRANCE_EXIT_SEPARATOR);
                Integer entrance = Integer.parseInt(entranceExit[0]);
                Integer exit = Integer.parseInt(entranceExit[1]);
                verifyWithinAllowedValues(entrance);
                verifyWithinAllowedValues(exit);
                if (entrance.compareTo(exit) == 0)
                    exception("No infinite ladders please");
                else
                    tunnels.put(entrance, exit);
            }
        } catch (NumberFormatException | NullPointerException e) {
            throw new RuntimeException("Invalid chutes and ladders configuration", e);
        }

        return tunnels;
    }

    private void verifyWithinAllowedValues(Integer position) {
        if (START_SQUARE.compareTo(position) > 0 || FINAL_SQUARE.compareTo(position) < 0)
            exception(String.format("Invalid chute or ladder with position '%s'", position));
    }

    public Integer nextImmediatePosition(Integer currentPosition, Integer spinnerValue) {
        verifyCurrentPosition(currentPosition);
        Integer nextPosition = currentPosition + spinnerValue;

        if (nextPosition.compareTo(FINAL_SQUARE) > 0)
            return currentPosition;
        else
            return nextPosition;
    }

    public Integer nextPositionIfChuteOrLadderUsed(Integer currentPosition, Integer spinnerValue) {
        Integer nextPosition = nextImmediatePosition(currentPosition, spinnerValue);
        return tunnels.getOrDefault(nextPosition, nextPosition);
    }

    private void verifyCurrentPosition(Integer currentPosition) {
        if (currentPosition.compareTo(FINAL_SQUARE) > 0 || currentPosition.compareTo(START_SQUARE) < 0)
            exception("How did you get there?");
    }

    private void exception(String message) {
        throw new RuntimeException(message);
    }
}
