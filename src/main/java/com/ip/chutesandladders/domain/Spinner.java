package com.ip.chutesandladders.domain;

import java.util.Random;

public class Spinner {
    private Random random = new Random();

    public Integer doTheMagic() {
        return random.nextInt(6) + 1;
    }
}