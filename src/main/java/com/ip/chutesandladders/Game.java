package com.ip.chutesandladders;

import com.ip.chutesandladders.domain.Board;
import com.ip.chutesandladders.domain.Move;
import com.ip.chutesandladders.domain.Player;
import com.ip.chutesandladders.domain.Spinner;
import com.ip.chutesandladders.util.CircularList;

import java.util.Comparator;

import static java.lang.String.format;

public class Game {
    public static final int MINIMUM_PLAYERS = 2;
    public static final int MAXIMUM_PLAYERS = 4;
    private Board board = new Board();
    private CircularList<Player> players = new CircularList<>();
    private Spinner spinner = new Spinner();
    private int lineNumber = 0;

    private void addPlayer(String name) {
        players.add(new Player(name, spinner.doTheMagic()));
    }

    private void play() {
        validatePlayers();
        firstRound();
        while (!nextMove(players.next())) {}
        declareWinner(players.current());
    }

    private void validatePlayers() {
        int count = players.size();
        if (count < MINIMUM_PLAYERS || count > MAXIMUM_PLAYERS)
            throw new IllegalArgumentException(format("There should be %s to %s players at the same time, not %s.",
                    MINIMUM_PLAYERS, MAXIMUM_PLAYERS, count));
    }

    private void declareWinner(Player player) {
        System.out.println("The winner is " + player.getName());
    }

    private void firstRound() {
        players.sort(new Comparator<Player>() {
            @Override
            public int compare(Player p1, Player p2) {
                return p2.getInitialSpinnerValue().compareTo(p1.getInitialSpinnerValue());
            }
        });

        for (int i = 0; i < players.size(); i++) {
            Player player = players.next();
            boolean finalPositionReached = firstMove(player);
            if (finalPositionReached) {
                declareWinner(players.current());
                return;
            }
        }
    }

    private boolean firstMove(Player player) {
        Integer oldPosition = player.getPawnPosition();
        Move move = player.movePawnForTheFirstTime(board);
        logMessage(player.getName(), oldPosition, move);

        return isFinalPositionReached(move);
    }

    private boolean nextMove(Player player) {
        Integer oldPosition = player.getPawnPosition();
        Move move = player.movePawn(board, spinner);
        logMessage(player.getName(), oldPosition, move);

        return isFinalPositionReached(move);
    }

    private void logMessage(String playerName, Integer previousPosition, Move move) {
        System.out.println(format("%s: %s: %s --> %s", lineNumber++, playerName, previousPosition, move.getNextPosition()) + chuteOrLadderMessage(move));
    }

    private String chuteOrLadderMessage(Move move) {
        String optionalMessage = "";
        if (move.getFinalPosition().compareTo(move.getNextPosition()) > 0)
            optionalMessage = " --LADDER--> " + move.getFinalPosition();
        else if (move.getFinalPosition().compareTo(move.getNextPosition()) < 0)
            optionalMessage = " --CHUTE--> " + move.getFinalPosition();
        return optionalMessage;
    }

    private boolean isFinalPositionReached(Move move) {
        return move.getFinalPosition().compareTo(100) == 0;
    }

    public static void main(String[] arguments) {
        Game game = new Game();
        game.addPlayer("Homer");
        game.addPlayer("Lisa");
        game.addPlayer("Bart");
        game.play();
    }
}