package com.ip.chutesandladders.util;

import java.util.LinkedList;

public class CircularList<T> extends LinkedList<T> {
    private int pointer = -1;

    public T current() {
        return get(pointer);
    }

    public T next() {
        if (isEmpty())
            throw new IllegalStateException("List is empty");

        pointer++;
        if (pointer == size()) pointer = 0;

        return get(pointer);
    }
}
