package com.ip.chutesandladders.domain;

import com.ip.chutesandladders.domain.Spinner;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class SpinnerTest {
    private List<Integer> expectedSteps = new LinkedList<>();

    @Before
    public void before() {
        expectedSteps.add(1);
        expectedSteps.add(2);
        expectedSteps.add(3);
        expectedSteps.add(4);
        expectedSteps.add(5);
        expectedSteps.add(6);
    }

    @Test
    public void shouldReturnNumbersBetweenOneAndSix() {
        Set<Integer> steps = new HashSet<>();
        Spinner spinner = new Spinner();

        int c = 10000;
        while (c > 0) {
            steps.add(spinner.doTheMagic());
            c--;
        }

        assertThat(steps).hasSize(6);
        assertThat(steps).containsAll(expectedSteps);
    }
}