package com.ip.chutesandladders.domain;

import com.ip.chutesandladders.domain.Board;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class BoardTest {
    Board emptyBoard = new Board("");

    @Test
    public void shouldReachFinalCellInHundredStepsOnBoardWithoutChutesOrLadders() {
        int stepsCount = 100;
        Integer currentPosition = 0;
        while (stepsCount > 0) {
            currentPosition = emptyBoard.nextPositionIfChuteOrLadderUsed(currentPosition, 1);
            stepsCount--;
        }

        assertThat(currentPosition).isEqualTo(100);
    }

    @Test
    public void shouldReachTopOfTheLadderPositionIfStoppedAtTheBottom() {
        Board board = new Board("1 50");
        assertThat(board.nextPositionIfChuteOrLadderUsed(0, 1)).isEqualTo(50);
    }

    @Test
    public void shouldNotSlideDownIfStoppedAtTheTopOfTheLadder() {
        Board board = new Board("1 50");
        assertThat(board.nextPositionIfChuteOrLadderUsed(49, 1)).isEqualTo(50);
    }

    @Test
    public void shouldSlideDownTheChuteIfStoppedAtTheTopOfTheChute() {
        Board board = new Board("50 1");
        assertThat(board.nextPositionIfChuteOrLadderUsed(49, 1)).isEqualTo(1);
    }

    @Test
    public void shouldNotClimbUpIfStoppedAtTheBottomOfTheChute() {
        Board board = new Board("50 1");
        assertThat(board.nextPositionIfChuteOrLadderUsed(0, 1)).isEqualTo(1);
    }

    @Test
    public void shouldThrowExceptionWhenLadderStartIsTooLow() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> new Board("-1 100")).withMessage("Invalid chute or ladder with position '-1'");
    }

    @Test
    public void shouldThrowExceptionWhenLadderEndIsTooHigh() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> new Board("1 101")).withMessage("Invalid chute or ladder with position '101'");
    }

    @Test
    public void shouldThrowExceptionWhenLadderConfigurationIsNotNumber() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> new Board("1 a")).withMessage("Invalid chutes and ladders configuration");
    }

    @Test
    public void shouldGoToPositionIncreasedBySpinnerValue() {
        int currentPosition = 10;
        int spinnerValue = 15;
        assertThat(emptyBoard.nextPositionIfChuteOrLadderUsed(currentPosition, spinnerValue)).isEqualTo(currentPosition + spinnerValue);
    }

    @Test
    public void shouldNotChangePositionIfNextPositionIsBeyondFinalSquare() {
        int currentPosition = 100;
        int spinnerValue = 15;
        assertThat(emptyBoard.nextPositionIfChuteOrLadderUsed(currentPosition, spinnerValue)).isEqualTo(currentPosition);
    }

    @Test
    public void shouldThrowExceptionWhenLadderStartEndEndsInTheSamePosition() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> new Board("50 50")).withMessage("No infinite ladders please");
    }

    @Test
    public void currentPositionShouldNotBeNegative() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> emptyBoard.nextPositionIfChuteOrLadderUsed(-1, 1)).withMessage("How did you get there?");
    }

    @Test
    public void currentPositionShouldNotBeBeyondBoard() {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> emptyBoard.nextPositionIfChuteOrLadderUsed(101, 1)).withMessage("How did you get there?");
    }
}