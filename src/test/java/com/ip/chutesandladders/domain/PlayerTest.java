package com.ip.chutesandladders.domain;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlayerTest {
    public static final int SPIN_BY_CONSTANT = 11;
    Player joe = new Player("joe", 1);
    Board board = new Board();

    @Mock
    Spinner oneSpinner;

    @Mock
    Spinner constantSpinner;

    @Before
    public void setUp() {
        when(oneSpinner.doTheMagic()).thenReturn(1);
        when(constantSpinner.doTheMagic()).thenReturn(SPIN_BY_CONSTANT);
    }

    @Test
    public void playerShouldEnterGameWithPositionOfZero() {
        assertThat(joe.getPawnPosition()).isEqualTo(Board.START_SQUARE);
    }

    @Test
    public void forTheFirstTimePlayerShouldMovePawnByInitialSpinnerValue() {
        Integer expectedPosition = new Integer(100);
        Player p = new Player("name", expectedPosition);
        assertThat(p.movePawnForTheFirstTime(board).getFinalPosition()).isEqualTo(expectedPosition);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenTryingToPerformFirstTimeMove() {
        joe.movePawnForTheFirstTime(board);
        joe.movePawnForTheFirstTime(board);
    }

    @Test
    public void shouldMovePawnByValueReturnedBySpinner() {
        Integer oldPosition = joe.getPawnPosition();
        Integer newPosition = joe.movePawn(new Board(""), constantSpinner).getNextPosition();
        assertThat(newPosition - oldPosition).isEqualTo(SPIN_BY_CONSTANT);
    }

    @Test
    public void playerPositionMayClimbTheLadder() {
        assertThat(joe.movePawn(board, oneSpinner).getFinalPosition()).isEqualTo(38);
    }

    @Test
    public void playerPositionMayGoDownTheChute() {
        while (joe.getPawnPosition() != 46) {
            joe.movePawn(board, oneSpinner);
        }

        assertThat(joe.movePawn(board, oneSpinner).getFinalPosition()).isEqualTo(26);
    }
}