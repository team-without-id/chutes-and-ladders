package com.ip.chutesandladders.util;

import org.junit.Test;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

public class CircularListTest {
    CircularList<String> circularList = new CircularList<>();

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenTryingToMoveOnEmptyList() {
        circularList.next();
    }

    @Test
    public void currentElementShouldBeAnElementFromLastStep() {
        circularList.add("one");
        circularList.add("two");

        assertThat(circularList.next()).isSameAs(circularList.current());
    }

    @Test
    public void shouldReturnElementsForever() {
        String aString = "I am the same string all the time";
        circularList.add(aString);

        IntStream.range(0, 100).forEach(x ->
                assertThat(circularList.next()).isSameAs(aString));
    }
}