# chutes-and-ladders

###### Assumptions:
- No concurrency required.
- There is no requirement discussed in case several players have the same initial spinner value. I've left it up to comparator.


###### Todo:
- When parsing entrance configuration try to provide more information on where exactly configuration is broken.
- Chutes/ladders configuration can be put into a file.
- More control on chutes/ladders configuration to not to allow one ladder exit to be a chute entrance. This might lead to a loop in the worst case.
- More control on players - don't allow the same player to be added twice.


###### Steps I'd make if it was a "real" project and other notes:
- Steps 0 to 7 and 50 to 57 look either like a bug or undocumented activity. The maximum number on spinner is 6. Discuss with business owner.
- Integers comparison is a bit complicated. This is necessary complication as for me though to avoid hard to discover issues with autoboxing.
- I did skip some of the obvious tests, e.g. creation of a player without a name, etc. I would not skip them in production code.
- Separate configuration parser from board logic or agree with "business" that single configuration exists only.
- Introduce interactive input for user count and names. Give user a second chance if something entered wrong instead of throwing an exception and halting right away.
- Create feature branches instead of pushing directly to master.
- Maybe add some more integration tests; tests for Game itself.
